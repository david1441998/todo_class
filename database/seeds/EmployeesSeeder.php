<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
           [
                'name' => 'Jacki',
                'email' =>'jack@jack.ibi',
                'password' =>Hash::make('123456'),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'employee'
           ],

           [
                'name' => 'Johni',
                'email' =>'Johni@John.ibi',
                'password' =>Hash::make('1234567'),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'employee'
           ],
                ]);
    }
}
