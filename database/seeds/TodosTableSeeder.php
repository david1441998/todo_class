<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('todos')->insert([
           [
                'id'=>1,
                'title' => 'Buy Milk',
                'user_id' =>1,
                'created_at' => date('Y-m-d G:i:s'),
                'status' =>0
           ],

           [
            'id'=>2,
            'title' => 'Prepare for the test',
            'user_id' =>2,
            'created_at' => date('Y-m-d G:i:s'),
            'status' =>0
            ],
            [
                'id'=>3,
                'title' => 'Read a book',
                'user_id' =>3,
                'created_at' => date('Y-m-d G:i:s'),
                'status' =>0
            ],     
                ]);
    }
}
