@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<h1>THE BUG IS FIXED</h1>
<h1>This is your todo list</h1>
<ul>

@foreach($todos as $todo)

@if ($todo->status)
           <input type = 'checkbox' id ="{{$todo->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$todo->id}}">
       @endif
<li>
    <a href = "{{route('todos.edit',$todo->id)}}">{{$todo->title}} </a> 
</li>
@endforeach
</ul>@can('manager')
//things only manager can see 


<a href = "{{route('todos.create')}}">create </a>@endcan
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               console.log('event.target.id');
               $.ajax({
                   url: "{{url('todos')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'PUT',
                   contentType: 'aplication/json',
                   data: JSON.stringify({'status': event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>
@endsection